package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/*change this method to testIsPalindrome()
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	  assert true and assert false */
	@Test //this method is going to fail bcz of testIsPalindrome 
		//returns false from the main method (see the red bar)
	public void testIsPalindrome () {
		assertTrue("Invalid value for palindrome" ,
		Palindrome.isPalindrome( "Anna" ) ) ;
	}
	
	/*now write enough code so that this first test passes in Palindrome.java
	 and run the main class as junit test - it should pass this time */
	/*next, write 3 more methos negative, boundary in and out, right here...
	 All the bellow tests must pass */
	
	@Test
	public void testIsPalindromeNegative() {
		assertFalse("Invalid value for palindrome",
		Palindrome.isPalindrome("Anna has a lamb"));
	}
		
	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid value for palindrome",
		Palindrome.isPalindrome("Aa"));		
	}
		
	@Test 
	public void testIsPalindromeBoundaryOut() {
	assertFalse("Invalid value for palindrome",
		Palindrome.isPalindrome("racers car"));	
	}
}
